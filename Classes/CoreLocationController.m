//
//  CoreLocationController.m
//  CoreLocationDemo
//
//  Created on 8/15/10.
//

#import "CoreLocationController.h"


@implementation CoreLocationController

@synthesize locMgr_precise, delegate,locMgr_easy,location_mgr,sec,velocity_arr,period,bgTask,background,expirationHandler,jobExpired,app,entry;

- (id)init {
	self = [super init];
	[self InitializeEasyLocationManager];
    //[self InitializePreciseLocationManager];
    
    self.app = [UIApplication sharedApplication];
    
    self.expirationHandler = ^{
        [app endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
        self.bgTask = [app beginBackgroundTaskWithExpirationHandler:expirationHandler];
        self.period = @"EXPIRED";
        self.jobExpired = YES;
        
        if(self.jobExpired) {
            // spin while we wait for the task to actually end.
            [NSThread sleepForTimeInterval:1];
        }
        
        // Restart the background task so we can run forever.
        //[self InitializeEasyLocationManager];
        [self startBackgroundTask];
    };
    
    //self.bgTask = [self.app beginBackgroundTaskWithExpirationHandler:expirationHandler];
    
    // Assume that we're in background at first since we get no notification from device that we're in background when
    // app launches immediately into background (i.e. when powering on the device or when the app is killed and restarted)
    //[self monitorLocationStateInBackground];
    
    self.background = YES;
    //[self startBackgroundTask];
    
	return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive)
    {
        [self StatusController:newLocation fromLocation:oldLocation];
        
        if (([self.location_mgr isEqual:@"drivetrack"])||([self.location_mgr  isEqual: @"normaltrack"]))
        {
            self.locMgr_easy.desiredAccuracy = kCLLocationAccuracyBest;
            self.locMgr_easy.distanceFilter = 1.0;
            self.background = NO;
        
            //[self.locMgr_easy stopUpdatingLocation];
            //[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(StartEasyTrack) userInfo:nil repeats:NO];
            
            NSLog(@"FOREGROUNDMODE");
        
        }
        
    }
    else if (UIApplication.sharedApplication.applicationState == UIApplicationStateBackground)
    {
         
        [self StatusController:newLocation fromLocation:oldLocation];
        
          if ([self.location_mgr isEqual:@"drivetrack"])
          {
              self.background = YES;
              self.locMgr_easy.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
              self.locMgr_easy.distanceFilter = 10.0;
              
              [self.locMgr_easy stopUpdatingLocation];
              [self startBackgroundTask];
              
              NSLog(@"BACKGROUNDMODE");
          }
          else if ([self.location_mgr  isEqual: @"normaltrack"])
          {
              self.background = YES;
              self.locMgr_easy.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
              self.locMgr_easy.distanceFilter = 10.0;
              
              [self startBackgroundTask];
              
              NSLog(@"BACKGROUNDMODE PER DIST");
          }
        
    }
    /*
    else if(UIApplication.sharedApplication.applicationState == UIApplicationStateInactive)
    {
        [self StatusController:newLocation fromLocation:oldLocation];
        
        if (([self.location_mgr isEqual:@"drivetrack"])||([self.location_mgr  isEqual: @"normaltrack"]))
        {
//            [self.locMgr_easy stopUpdatingLocation];
//            [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(StartEasyTrack) userInfo:nil repeats:NO];
//            
//            NSLog(@"INACTIVEMODE");
        }
    
    }
    */
    
    if ((sec > 0) &&([newLocation speed] > 2) && ([self.velocity_arr count]!=0))
    {
        self.sec = 0;
        [self.velocity_arr removeAllObjects];
    }
    
}


- (void)startBackgroundTask

{
    
//    self.app = [UIApplication sharedApplication];
//    
//    self.expirationHandler = ^{
//        [app endBackgroundTask:self.bgTask];
//        self.bgTask = UIBackgroundTaskInvalid;
//        self.bgTask = [app beginBackgroundTaskWithExpirationHandler:expirationHandler];
//        self.period = @"EXPIRED";
//        self.jobExpired = YES;
//        
//        if(self.jobExpired) {
//            // spin while we wait for the task to actually end.
//            [NSThread sleepForTimeInterval:1];
//        }
//        
//        // Restart the background task so we can run forever.
//        //[self InitializeEasyLocationManager];
//        [self startBackgroundTask];
//    };
//    
      self.bgTask = [self.app beginBackgroundTaskWithExpirationHandler:expirationHandler];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // When the job expires it still keeps running since we never exited it. Thus have the expiration handler
        // set a flag that the job expired and use that to exit the while loop and end the task.
        
        if(self.background && !self.jobExpired)
        {
            if ([self.location_mgr isEqual:@"drivetrack"])
            {
                [self StartEasyTrack];
                [NSThread sleepForTimeInterval:15.0];
            }
            else
            {
                [self StartEasyTrack];
            }

//            NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(StartEasyTrack) userInfo:nil repeats:NO];
//            
//           [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
 
        }
        
        self.jobExpired = NO;
        
    });
    
}


-(void)StatusController:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    
    if([self.delegate conformsToProtocol:@protocol(CoreLocationControllerDelegate)])
    {
        [self.delegate locationUpdate:newLocation fromLocation:oldLocation];
    }
    
   // NSLog(@"%@",[NSString stringWithFormat:@"SPEED: %.02f",[newLocation speed]]);
    
    
    //Sometimes the first location is cached and it doesn't represent the actual location
    if ((newLocation.horizontalAccuracy < 0)||(newLocation.verticalAccuracy <0)||([newLocation speed] < 0))
        return;

    
    if (([newLocation speed ] > 11) && ([self.location_mgr  isEqual: @"normaltrack"])) //24.6mph
    {
        self.location_mgr = @"drivetrack";
    }
    else if (([newLocation speed ] < 6)&& ([self.location_mgr isEqual: @"drivetrack"]))
    {
        [self StopEasyTrack];
        [self InitializePreciseLocationManager];
        [self StartPreciseTrack];
    }
    else if (([newLocation speed ] > 10) && ([self.location_mgr  isEqual: @"precisetrack"]))
    {
        [self StopPreciseTrack];
        [self InitializeEasyLocationManager];
        [self StartEasyTrack];
        self.location_mgr = @"drivetrack";
    }
    else  if (([newLocation speed] < 2) && ([self.location_mgr  isEqual: @"precisetrack"])) //8.95mph
    {
        
        //[self.velocity_arr addObject: [NSNumber numberWithDouble:[newLocation speed]]];
        //self.temp_speed = [[self.velocity_arr objectAtIndex: sec] doubleValue];
        
        [self.velocity_arr addObject: newLocation];
        
        sec = sec + 1;
        
        if (sec > 60)
        {
            [self StopPreciseTrack];  //stop updating location first
            
            [self droptable: @"coordinate"];
            
            NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
            [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            
            for (int i =0;i <[self.velocity_arr count];i++)
            {
                CLLocation *curr = [self.velocity_arr objectAtIndex: i];
                
                if([curr speed] < 1)
                {
                    NSString* date = [NSString stringWithFormat:@"%@", [DateFormatter stringFromDate:[curr timestamp]]];
                    NSString* latitude = [NSString stringWithFormat:@"%f", curr.coordinate.latitude];
                    NSString* longtitude = [NSString stringWithFormat:@"%f", curr.coordinate.longitude];
                    NSString* velocity = [NSString stringWithFormat:@"%f", [curr speed]];
                    
                    [self save: velocity
                      withDate: date
                      withLati: latitude
                      withLong: longtitude ];
                    
                    break;
                }
            }
            
            [self.velocity_arr removeAllObjects];
            [self InitializeEasyLocationManager];
            [self StartEasyTrack];
            self.location_mgr = @"normaltrack";
        }
        
    }
}

-(void)StartEasyTrack{
        [self.locMgr_easy startUpdatingLocation];
        self.sec = 0;
        self.period = @"per 15 secs";
       // self.location_mgr = @"normaltrack";
}

/*
-(void)StartPeriodicallyTrack{
    [self.locMgr_easy startUpdatingLocation];
    self.period = @"periodic normal";
}
*/

- (void)StopEasyTrack{
        [self.locMgr_easy stopUpdatingLocation];
        locMgr_easy.delegate = nil;
        locMgr_easy = nil;
        [locMgr_easy release];
}
    
    
- (void)StartPreciseTrack{
        [self.locMgr_precise startUpdatingLocation];
        self.location_mgr = @"precisetrack";
        self.period = @"per sec";
        
}
    
    
- (void)StopPreciseTrack{
        [self.locMgr_precise stopUpdatingLocation];
        locMgr_precise.delegate = nil;
        locMgr_precise = nil;
        [locMgr_precise release];
}
    
    
//Easy location manager
- (void)InitializeEasyLocationManager{
        if(self != nil) {
            self.locMgr_easy = [[[CLLocationManager alloc] init] autorelease];
            self.locMgr_easy.delegate = self;
 
            //self.locMgr_easy.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
            self.locMgr_easy.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            self.locMgr_easy.activityType = CLActivityTypeAutomotiveNavigation;
            self.locMgr_easy.pausesLocationUpdatesAutomatically = YES;
            //self.locMgr_easy.distanceFilter = 3000.0;
            self.locMgr_easy.distanceFilter = 10.0;
        }
    
}


//Precise location manager
- (void)InitializePreciseLocationManager{
    if(self != nil){
        self.locMgr_precise = [[[CLLocationManager alloc] init] autorelease];
        self.locMgr_precise.delegate = self;
        self.locMgr_precise.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        self.velocity_arr = [[NSMutableArray alloc] initWithCapacity:0];
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	if([self.delegate conformsToProtocol:@protocol(CoreLocationControllerDelegate)]) {
		[self.delegate locationError:error];
	}
}


-(void)createTable: (NSString *) tableName
    withField1: (NSString *) field1
    withField2: (NSString *) field2
    withField3: (NSString *) field3
    withField4: (NSString *) field4
{
    char *err;
    NSString *sql = [NSString stringWithFormat:
                     @"CREATE TABLE IF NOT EXISTS '%@' ( '%@'"
                     "TEXT PRIMARY KEY, '%@' TEXT, '%@' TEXT, '%@' TEXT);", tableName ,field1,
                     field2, field3, field4];
    
    if(sqlite3_exec(db,[sql UTF8String],NULL,NULL,&err)!= SQLITE_OK)
    {
        sqlite3_close(db);
        NSAssert(0,@"Could not create table");
    }
    else
    {
        NSLog(@"table created");
    }
    
}



- (void)save: (NSString *) velocity
    withDate: (NSString *) date
    withLati: (NSString *) latitude
    withLong: (NSString *) longtitude
{
    
        const char *filepath = (const char *) [self.filepath UTF8String];
        const char *velocity_c = (const char *) [velocity UTF8String];
        const char *date_c = (const char *) [date UTF8String];
        const char *latitude_c = (const char *) [latitude UTF8String];
        const char *longtitude_c = (const char *) [longtitude UTF8String];

        sqlite3_stmt *stmt=nil;
        
        //insert
        const char *sql = "INSERT INTO coordinate ('Date','Latitude','Longtitude','Velocity') VALUES (?, ?, ?, ?)";
        
        //Open db
        if(sqlite3_open(filepath,&db)==SQLITE_OK)
        {
            if (sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) ==SQLITE_OK ){
                sqlite3_bind_text(stmt, 1, date_c, -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(stmt, 2, latitude_c,-1, SQLITE_TRANSIENT);
                sqlite3_bind_text(stmt, 3, longtitude_c,-1,SQLITE_TRANSIENT);
                sqlite3_bind_text(stmt, 4, velocity_c,-1,SQLITE_TRANSIENT);
                // sqlite3_bind_int(stmt, 2, integer);
                // sqlite3_bind_double(stmt, 3, dbl);
                
                if (sqlite3_step(stmt) == SQLITE_DONE)
                    sqlite3_finalize(stmt);
                else
                    printf("%s",sqlite3_errmsg(db));
                
            }
            else{
                
                NSAssert(0, @"could not update table");
            }
        
            sqlite3_close(db);
        }

}


- (void)droptable: (NSString *) table
{
    
     NSString *sql = [NSString stringWithFormat:@"DELETE FROM '%@';",table];
     
     char *err;
     if(sqlite3_exec(db,[sql UTF8String],NULL,NULL,&err)!=SQLITE_OK)
     {
         sqlite3_close(db);
         NSAssert(0, @"could not drop table");
     }
     else
     {
         NSLog(@"table drop");
     }
    
}


-(NSString*)table_rows_count
{
    //return the number of rows in section.
    return [entry count];
}


-(void)query_table
{
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM coordinate"];
    sqlite3_stmt *statement;
    
    if(sqlite3_prepare_v2(db, [sql UTF8String], -1, &statement, nil)==SQLITE_OK)
    {
        while(sqlite3_step(statement)==SQLITE_ROW)
        {
            char *date = (char*) sqlite3_column_text(statement, 0);
            NSString *datestr = [[NSString alloc]initWithUTF8String:date];//timestamp
            
            char *longtitude = (char*) sqlite3_column_text(statement, 1);
            NSString *longtitudestr = [[NSString alloc]initWithUTF8String:longtitude];
            
            char *latitude = (char*) sqlite3_column_text(statement, 2);
            NSString *latitudestr = [[NSString alloc]initWithUTF8String:latitude];
            
            char *speed = (char*) sqlite3_column_text(statement, 3);
            NSString *speedstr = [[NSString alloc]initWithUTF8String:speed];
            
            NSString *str = [[NSString alloc]initWithFormat:@" %@ %@/%@ - %@", datestr,longtitudestr,latitudestr,speedstr];
            
            [entry addObject:str];
        }
        
    }
}



-(NSString *)get_table_content
{
    //configure the cell
    cell = [self.entry objectAtIndex:0];
    return cell;
}


//filepath to db
-(NSString *) filepath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths objectAtIndex:0]stringByAppendingPathComponent:@"location.sql"];
}


//open db
-(void)openDB
{
    if(sqlite3_open([[self filepath]UTF8String], &db)!=SQLITE_OK)
    {
        sqlite3_close(db);
        NSAssert(0,@"Database failed to open");
    }
    else
    {
        NSLog(@"database open");
    }
}


- (void)dealloc {
	[self.locMgr_easy release];
    [self.locMgr_precise release];
    [self.velocity_arr release];
	[super dealloc];
}


@end
