//
//  CoreLocationDemoAppDelegate.h
//  CoreLocationDemo
//
//  Created by Nicholas Vellios on 8/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CoreLocationDemoViewController;
@class CoreLocationController;

@interface CoreLocationDemoAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    CoreLocationDemoViewController *viewController;
    CoreLocationController *CLController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet CoreLocationDemoViewController *viewController;
@property (nonatomic, retain) CoreLocationController *CLController;

@end

