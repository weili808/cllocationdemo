//
//  CoreLocationDemoViewController.m
//  CoreLocationDemo
//
//  Created on 8/15/10.
//

#import "CoreLocationDemoViewController.h"

@implementation CoreLocationDemoViewController{
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@synthesize CLController,motionManager,bgTask_view,background,expirationHandler_view,jobExpired,app_view;

#define kAccelerometerFrequency        50.0 //Hz
#define kFilteringFactor 0.1
#define to_mph 2.23693629
#define kUpdateFrequency    60.0

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];

    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .2;
    
    currentMaxvector = 0;
    currentMaxvelocity = 0;
        
    //self.motionManager.accelerometerUpdateInterval = 1.0 / kUpdateFrequency;
    
    px = py = pz = 0;
    numSteps = 0;
    
    
	CLController = [[CoreLocationController alloc] init];
	CLController.delegate = self;
    
    [CLController openDB];
    [CLController createTable:@"coordinate" withField1:@"Date" withField2:@"Latitude" withField3:@"Longtitude" withField4:@"Velocity"];
    
    CLController.entry = [[NSMutableArray alloc] init ];
    [CLController query_table];
    
    if(CLController.table_rows_count == 0)
    {
        row = @"no entry";
    }
    else
    {
        row = CLController.get_table_content;
    }
    
    [CLController StartEasyTrack];
    CLController.location_mgr = @"normaltrack";
  //[CLController StartPreciseTrack];
    self.update_status=@"init update";
    
    
    self.app_view = [UIApplication sharedApplication];
    
    self.expirationHandler_view = ^{
        [app_view endBackgroundTask:self.bgTask_view];
        self.bgTask_view = UIBackgroundTaskInvalid;
        self.bgTask_view = [app_view beginBackgroundTaskWithExpirationHandler:expirationHandler_view];
        self.jobExpired = YES;
        
        if(self.jobExpired) {
            // spin while we wait for the task to actually end.
            [NSThread sleepForTimeInterval:1];
        }
        
        // Restart the background task so we can run forever.
        //[self InitializeEasyLocationManager];
        [self startBackgroundTask];
    };
    
    self.background = YES;
    

    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [self outputAccelertionData:accelerometerData.acceleration];
                                                 });
                                                 if(error){
                                                     
                                                     NSLog(@"%@", error);
                                                 }
                                             }];
    
}


- (void)locationUpdate:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    /*
    int distanceThreshold = 15.0; // in meters
    NSString *status = @"less than 15m/s";
    if ([newLocation distanceFromLocation:oldLocation] > distanceThreshold)
    {
        status = @"more than 15m/s";
    }
    */
    
    //to get the time in background (crash if put it in background section)
    NSTimeInterval interval = [newLocation.timestamp timeIntervalSinceNow];
    float interval_min = -(interval)/60;
    self.update_status = [NSString stringWithFormat:@"backgound %.02f minutes",interval_min];
    

    NSString *s_speed ;
    
    if (([newLocation speed ]> 11) && (([CLController.location_mgr isEqual: @"normaltrack"])
                                            ||([CLController.location_mgr isEqual: @"drivetrack"])))
        s_speed = @"Driving";
    else if (([newLocation speed] < 8) && ([CLController.location_mgr isEqual: @"drivetrack"]))
        s_speed = @"Ready to Stop";
    else if (([newLocation speed] < 2) && ([CLController.location_mgr  isEqual: @"precisetrack"]))
        s_speed = @"Get Stop Location";
    else if (([newLocation speed] > 3) && ([CLController.location_mgr  isEqual: @"precisetrack"]))
        s_speed = @"Back to Drivng";
    else
        s_speed = @"Roaming";
    
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
	Old_t_Label.text = [NSString stringWithFormat:@"OLD_TIME: %@", [DateFormatter stringFromDate:[oldLocation timestamp]]];
    New_t_Label.text = [NSString stringWithFormat:@"NEW_TIME: %@", [DateFormatter stringFromDate:[newLocation timestamp]]];
	
  //Old_v_Label.text = [NSString stringWithFormat:@"ARR_COUNT: %d", [CLController.velocity_arr count]];
    Old_v_Label.text = [NSString stringWithFormat:@"SPEED: %.02fm/h   %@",[newLocation speed]*2.2369,[CLController period]];
	New_v_Label.text = [NSString stringWithFormat:@"MANAGER: %@", CLController.location_mgr];
    
    status_v_Label.text = [NSString stringWithFormat:@"STATUS: %@",s_speed ];
  //status_a_Label.text = [NSString stringWithFormat:@"Loc_mgr: %@", CLController.location_mgr];
    status_a_Label.text = [NSString stringWithFormat:@"SIGNAL:(V) %.02f  (H) %.02f", newLocation.verticalAccuracy, newLocation.horizontalAccuracy];
    
  //  lati_Label.text = [NSString stringWithFormat:@"ALTITUDE: %f", newLocation.altitude];
    
  //lati_Label.text = [NSString stringWithFormat:@"LATITUDE: %f", newLocation.coordinate.latitude];
  //long_Label.text = [NSString stringWithFormat:@"LONGITUDE: %f", newLocation.coordinate.longitude];
  //long_Label.text = [NSString stringWithFormat:@"UPDATE: %@", self.update_status];
    
 //   alti_Label.text = [NSString stringWithFormat:@"SECONDS/ROW: %d  %@", CLController.sec, row];
    
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            lati_Label.text = [NSString stringWithFormat:@"%@ %@ %@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                               placemark.postalCode];
            
            alti_Label.text = [NSString stringWithFormat:@"%@ %@ %@",
                               placemark.locality,
                               placemark.administrativeArea,
                               placemark.country];

        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];

    
    
    
    /*
    if ([CLController state] == UIApplicationStateActive)
        self.update_status=@"active update";
    else
        self.update_status =@"enter background";
    */
    
    //else if ((state == UIApplicationStateBackground) || (state == UIApplicationStateInactive))
        //self.update_status =@"enter background";
    //{
        /*
        if (([CLController.location_mgr isEqual:@"drivetrack"])||([CLController.location_mgr  isEqual: @"normaltrack"]))
        {
            [CLController.locMgr_easy stopUpdatingLocation];
            [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(StartEasyTrack) userInfo:nil repeats:NO];
        }
        */
    //}

    
    
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    //NSLog(@"Pausing location Updates");
    //self.update_status =@"pause update";
    
     UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Did Pause Update" message:@"enter pause" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
     [alert show];
     [alert release];
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    //NSLog(@"Resuming location Updates");
    //self.update_status=@"resume update";
    
     UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Did Resume Update" message:@"back resume" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
     [alert show];
     [alert release];
}




-(void)outputAccelertionData:(CMAcceleration)acceleration
{
    
    double accX_2 = powf(acceleration.x,2);
    double accY_2 = powf(acceleration.y,2);
    double accZ_2 = powf(acceleration.z,2);
    
    
    double vectorSum = sqrt(accX_2 + accY_2 + accZ_2);
    
    double velocity = 2.23693629*vectorSum;
    
    
    if(fabs(vectorSum) > fabs(currentMaxvector))
        currentMaxvector = vectorSum;
    if(fabs(velocity) > fabs(currentMaxvelocity))
        currentMaxvelocity = velocity;
    
    
    
    float xx = acceleration.x;
    float yy = acceleration.y;
    float zz = acceleration.z;
    
    float dot = (px * xx) + (py * yy) + (pz * zz);
    float a = ABS(sqrt(px * px + py * py + pz * pz));
    float b = ABS(sqrt(xx * xx + yy * yy + zz * zz));
    
    dot /= (a * b);
    
    if (dot <= 0.82) {
        if (!isSleeping) {
            isSleeping = YES;
            [self performSelector:@selector(wakeUp) withObject:nil afterDelay:0.3];
            numSteps += 1;
        }
    }
    
    long_Label.text = [NSString stringWithFormat:@"MaxVelocity/Steps:   %.02f     %d", currentMaxvelocity, numSteps];
    
    px = xx; py = yy; pz = zz;
    
}



- (void)startBackgroundTask

{
    self.bgTask_view = [self.app_view beginBackgroundTaskWithExpirationHandler:expirationHandler_view];
    
    
    if(self.motionManager==nil)
    {
        self.motionManager=[[CMMotionManager alloc] init];
    }
    
    self.motionManager.accelerometerUpdateInterval= 1;
    
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if(self.background && !self.jobExpired)
             {
                 [self outputAccelertionData:accelerometerData.acceleration];
             }
         });
         
         if(error){
             NSLog(@"%@", error);
         }
         self.jobExpired = NO;
     }];
    
}




- (void)wakeUp {
    isSleeping = NO;
}


- (IBAction)reset:(id)sender {
    numSteps = 0;
    currentMaxvelocity = 0;
}




- (void)locationError:(NSError *)error {
	Old_t_Label.text = [error description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return YES;
}

- (void)viewDidUnload {
}

- (void)dealloc {
	[CLController release];
    [_reset release];
    [super dealloc];
}

@end
