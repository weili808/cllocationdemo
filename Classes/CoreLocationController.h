//
//  CoreLocationController.h
//  CoreLocationDemo
//
//  Created on 8/15/10.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "sqlite3.h"

@protocol CoreLocationControllerDelegate
@required

- (void)locationUpdate:(CLLocation *)newlocation fromLocation
                      :(CLLocation *)oldlocation;

- (void)locationError:(NSError *)error;

@end


@interface CoreLocationController : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *locMgr_easy;
    CLLocationManager *locMgr_precise;
    NSString *location_mgr;
	id delegate;
    NSInteger sec;
    NSMutableArray *velocity_arr;
    sqlite3 *db;
    NSString *period;
    NSString *cell;
    
}

@property (nonatomic, retain) CLLocationManager *locMgr_easy;
@property (nonatomic, retain) CLLocationManager *locMgr_precise;
@property (nonatomic, retain) NSString *location_mgr;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) NSInteger sec;
@property (nonatomic, assign) NSMutableArray *velocity_arr;
@property (nonatomic, assign) NSString *period;



@property (nonatomic, assign) UIBackgroundTaskIdentifier bgTask;
@property (assign, nonatomic) BOOL background;
@property (strong, nonatomic) dispatch_block_t expirationHandler;
@property (assign, nonatomic) BOOL jobExpired;
@property (assign, nonatomic) UIApplication* app;

@property (nonatomic, retain) NSMutableArray *entry;

- (void)StartEasyTrack;
- (void)StopEasyTrack;
- (void)StartPreciseTrack;
- (void)StopPreciseTrack;
- (void)InitializeEasyLocationManager;
- (void)InitializePreciseLocationManager;


- (void)StatusController: (CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

-(void)createTable: (NSString *) tableName
        withField1: (NSString *) field1
        withField2: (NSString *) field2
        withField3: (NSString *) field3
        withField4: (NSString *) field4;


- (void)save: (NSString* ) velocity
    withDate: (NSString *) date
    withLati: (NSString *) latitude
    withLong: (NSString *) longtitude;

- (void)droptable: (NSString *) table;

-(NSString *) filepath;
-(void)openDB;

- (void)startBackgroundTask;


-(NSString*) get_table_content;
-(NSString*) table_rows_count;
-(void)query_table;

@end
