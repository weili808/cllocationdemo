//
//  CoreLocationDemoViewController.h
//  CoreLocationDemo
//
//  Created by Nicholas Vellios on 8/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "CoreLocationController.h"

@interface CoreLocationDemoViewController : UIViewController <CoreLocationControllerDelegate> {
	CoreLocationController *CLController;
	IBOutlet UILabel *Old_t_Label;
	IBOutlet UILabel *New_t_Label;
	IBOutlet UILabel *Old_v_Label;
	IBOutlet UILabel *New_v_Label;
    IBOutlet UILabel *status_v_Label;
    IBOutlet UILabel *status_a_Label;
    IBOutlet UILabel *long_Label;
    IBOutlet UILabel *lati_Label;
    IBOutlet UILabel *alti_Label;
    
    double currentMaxvector;
    double currentMaxvelocity;
    
    float px;
    float py;
    float pz;
    int numSteps;
    BOOL isSleeping;
    NSString *row;
}

@property (nonatomic, retain) CoreLocationController *CLController;
@property (nonatomic, assign) NSString* update_status;


@property (strong, nonatomic) CMMotionManager *motionManager;


@property (nonatomic, assign) UIBackgroundTaskIdentifier bgTask_view;
@property (assign, nonatomic) BOOL background;
@property (strong, nonatomic) dispatch_block_t expirationHandler_view;
@property (assign, nonatomic) BOOL jobExpired;
@property (assign, nonatomic) UIApplication* app_view;


- (IBAction)reset:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *reset;

-(void)outputAccelertionData:(CMAcceleration)acceleration;

- (void)startBackgroundTask;

@end

